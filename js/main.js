class miTabla extends HTMLElement {
  constructor() {
    //Creación del templete
    const tpl = document.querySelector("#templete-tabla");
    const tplInst = tpl.content.cloneNode(true);
    super();
    this.attachShadow({ mode: "open" });
    this.shadowRoot.appendChild(tplInst);
  }
  //Al insertse el elemento creado
  connectedCallback() {
      this.hacerTabla();
  }
  //atributos a observar
  static get observedAttributes(){
    return ['buscar_id'];
  }
  //si hay un id nuevo a buscar
  attributeChangedCallback(attrName, oldVal, newVal){
    if(attrName === 'buscar_id'){
      this.hacerTabla(newVal);
    }
  }



  hacerTabla(id = '') {
      try {
        const listaEmpleados = fetch(
            "http://dummy.restapiexample.com/api/v1/employees"
          )
            .then((response) => response.json())
            .catch((error) => {console.log(error);
                alert('Problemas con la API intente nuevamente.')
            })
            .then((data) => {
                let listaEmpleados = this.shadowRoot.querySelector("tbody");
                listaEmpleados.innerHTML = "";
    
                if(id !== ''){
                    let contador =0;
                    //pintamos solo al que corresponde
                    for (let item of data.data) {
                        if(item.id== id){
                        listaEmpleados.innerHTML += `
                        <tr id="${item.id}">
                        <td>${item.id}</td>
                        <td>${item.employee_name}</td>
                        <td>${item.employee_salary}</td>
                        <td>${item.employee_age}</td>
                        </tr>
                        `;
                        contador++;
                        break;
                        }   
                    }
                    //si no hay consicencias mandamos mensaje
                    if(contador==0){
                        listaEmpleados.innerHTML +=`
                        <p>No se encontro ningún empleado con ese id</p>
                        `
                    }
                }else{
                    //pintamos todos los datos de los empleados
                    for (let item of data.data) {
                        listaEmpleados.innerHTML += `
                        <tr id="${item.id}">
                        <td>${item.id}</td>
                        <td>${item.employee_name}</td>
                        <td>${item.employee_salary}</td>
                        <td>${item.employee_age}</td>
                        </tr>
                        `;
                    }
                }
            });
      } catch (error) {
          alert('Problemas con la API intente nueva mente.')
      }
    
  }
}
customElements.define("mi-tabla", miTabla);

class miBuscador extends HTMLElement {
  constructor() {
      //creación del componente templete
    const tpl = document.querySelector("#templete-buscador");
    const tplInst = tpl.content.cloneNode(true);
    super();
    this.attachShadow({ mode: "open" });
    this.shadowRoot.appendChild(tplInst);
    //evento para el boton buscar
    this.shadowRoot.querySelector("#buscar").addEventListener("click", (e) => {
        let id_buscar= this.shadowRoot.getElementById('buscadorE').value;
        //modifica el valor de la propiedad por lo que se activa el evento en el componente mi-tabla
        document.querySelector('mi-tabla').setAttribute('buscar_id', id_buscar);
    });
    this.shadowRoot.querySelector("#limpiar").addEventListener("click", (e) => {
    this.shadowRoot.querySelector("#buscadorE").value = "";
    });
  }
}

customElements.define("mi-buscador", miBuscador);
